// Fill out your copyright notice in the Description page of Project Settings.

#include "Spawner.h"
#include "Food.h"
#include <ctime>
#include <cstdlib>



// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CollisionMesh = CreateDefaultSubobject<UBoxComponent>(FName("Collision Mesh"));
	FTransform NewTransofrm = CollisionMesh->GetRelativeTransform();
	CollisionMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	FMath::RandRange(GetActorLocation().X - CollisionMesh->GetComponentLocation().X, GetActorLocation().X + CollisionMesh->GetComponentLocation().X);

}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();
	SpawnObject();
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}



float ASpawner::GetRandSpawnYPoint()
{
	float y = FMath::RandRange((GetActorLocation().Y -(CollisionMesh->GetCollisionShape().GetExtent().Y)), (GetActorLocation().Y + CollisionMesh->GetCollisionShape().GetExtent().Y));
	return y;
}

float ASpawner::GetRandSpawnXPoint()
{
	float x = FMath::RandRange((GetActorLocation().X + -(CollisionMesh->GetCollisionShape().GetExtent().X)) , (GetActorLocation().X  + CollisionMesh->GetCollisionShape().GetExtent().X));

	return x;
}


void ASpawner::SpawnObject()
{
	float x = GetRandSpawnXPoint();
	float y = GetRandSpawnYPoint();
	FVector NewLocation = FVector(x , y , 0 );
	FTransform SpawnTransform = FTransform(NewLocation);
	AFood* NewFood = GetWorld()->SpawnActor<AFood>(ToSpawn, SpawnTransform);
	NewFood->OnFoodActivated.AddDynamic(this, &ASpawner::SpawnObject);


}


