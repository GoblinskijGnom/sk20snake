// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Components/BoxComponent.h"
#include "TeleportBorder.generated.h"

class UStaticMeshComponent;
class ASnakeBase;
class ASnakeElementBase;

UCLASS()
class SNAKEGAME_API ATeleportBorder : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATeleportBorder();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* Chell;

	UPROPERTY(EditAnywhere)
		float TpYLength;
	
	UPROPERTY(EditAnywhere)
		float TpXLength;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
