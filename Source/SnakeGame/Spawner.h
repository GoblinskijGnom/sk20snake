// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Spawner.generated.h"

class AFood;
class ALevelBorder;

UCLASS()
class SNAKEGAME_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();


	UPROPERTY(EditAnywhere) 
	UBoxComponent* CollisionMesh;

	UBoxComponent * RootComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Spawning")
	TSubclassOf<AFood> ToSpawn;

	UPROPERTY()
	FTransform NewTransform;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	float GetRandSpawnXPoint();

	float GetRandSpawnYPoint();



	UFUNCTION()
	void SpawnObject();

};
