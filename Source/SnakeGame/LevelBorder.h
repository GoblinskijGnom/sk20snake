// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Components/BoxComponent.h"
#include "LevelBorder.generated.h"

class ASnakeElementBase;
class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ALevelBorder : public AActor, public IInteractable
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	ALevelBorder();


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* BorderMeshComponent;

	FTimerHandle MyTimerHandle;

	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	void BordersOn();

};
