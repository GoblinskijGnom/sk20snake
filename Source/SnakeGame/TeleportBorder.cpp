// Fill out your copyright notice in the Description page of Project Settings.


#include "TeleportBorder.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"


// Sets default values
ATeleportBorder::ATeleportBorder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Chell = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PortalComponent"));
	Chell->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	
}

// Called when the game starts or when spawned
void ATeleportBorder::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATeleportBorder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATeleportBorder::Interact(AActor* Interactor, bool bIsHead)
{

	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		ASnakeElementBase* BPFirst = (Snake->SnakeElements[0]);

		if (IsValid(Snake))
		{
			FVector CurrnetLocation = BPFirst->GetActorLocation();
			FVector NewLocation = CurrnetLocation - FVector(TpXLength, TpYLength, 0);
			BPFirst->SetActorLocation(NewLocation);
			
		}
	}
}
