// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelBorder.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "SnakeElementBase.h"

// Sets default values
ALevelBorder::ALevelBorder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	BorderMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	BorderMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	BorderMeshComponent->SetHiddenInGame(true);
}

// Called when the game starts or when spawned
void ALevelBorder::BeginPlay()
{
	Super::BeginPlay();
	GetWorld()->GetTimerManager().SetTimer(MyTimerHandle, this, &ALevelBorder::BordersOn, 150.f, false);
}

// Called every frame
void ALevelBorder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelBorder::BordersOn()
{
	BorderMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BorderMeshComponent->SetHiddenInGame(false);
}

void ALevelBorder::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}


